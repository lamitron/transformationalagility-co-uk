import React from "react";
import Login from "../components/login";
import {Footer} from "../components/footer";

export default function login() {
    return (
        <>
            <Login />
            <Footer />
        </>
    );
}